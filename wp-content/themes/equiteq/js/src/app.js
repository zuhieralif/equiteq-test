$(document).ready(function() {
    var selectedIndustry = 'default-sectors';
    var selectedLocation = 'default-locations';

    $('.dropdown-menu a').click(function(event) {
        event.preventDefault();
        var selectedOption = $(this).text();
        $(this).closest('.dropdown').find('.dropdown-toggle').text(selectedOption);
    });

    $('.dropdown-menu[aria-labelledby="industryDropdown"] .dropdown-item').on('click', function() {
        selectedIndustry = $(this).data('industry');
        filterTeamBox();
    });

    $('.dropdown-menu[aria-labelledby="locationDropdown"] .dropdown-item').on('click', function() {
        selectedLocation = $(this).data('location');
        filterTeamBox();
    });

    $('#quicksearch').on('keyup', function() {
        var searchTerm = $(this).val().toLowerCase();
        $('.team-box').each(function() {
            var name = $(this).text().toLowerCase();
            var industries = $(this).data('industry').split(',').map(function(industry) { return industry.trim(); });
            var industryMatch = selectedIndustry === 'default-sectors' || industries.includes(selectedIndustry);
            var locationMatch = selectedLocation === 'default-locations' || $(this).data('location') === selectedLocation;
            if (name.indexOf(searchTerm) !== -1 && industryMatch && locationMatch) {
                $(this).css({ 'max-height': '1000px', 'max-width': '1000px', 'opacity': '1', 'margin-bottom': '', 'overflow': 'hidden' });
            } else {
                $(this).css({ 'max-height': '0', 'max-width': '0', 'opacity': '0', 'margin-bottom': '0', 'overflow': 'hidden' });
            }
        });
    });        

    function filterTeamBox() {
        var searchTerm = $('#quicksearch').val().toLowerCase();
        $('.team-box').each(function() {
            var name = $(this).text().toLowerCase();
            var industries = $(this).data('industry').split(',').map(function(industry) { return industry.trim(); });
            var industryMatch = selectedIndustry === 'default-sectors' || industries.includes(selectedIndustry);
            var locationMatch = selectedLocation === 'default-locations' || $(this).data('location') === selectedLocation;
            var searchMatch = name.indexOf(searchTerm) !== -1;
            if (industryMatch && locationMatch && searchMatch) {
                $(this).css({ 'padding': '0 15px', 'max-height': '1000px', 'max-width': '1000px', 'opacity': '1', 'margin-bottom': '', 'overflow': 'hidden' });
            } else {
                $(this).css({ 'padding': '0', 'max-height': '0', 'max-width': '0', 'opacity': '0', 'margin-bottom': '0', 'overflow': 'hidden' });
            }
        });
    }    

    filterTeamBox();  // Show all experts by default
});
