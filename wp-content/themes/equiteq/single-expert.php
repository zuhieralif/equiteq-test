<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);
// $industry_expertises = maybe_unserialize($expert->industry_expertise);
$profile_image = get_field('profile_image');
$position_title = get_field('position_title');
$expert_email = get_field('expert_email');
$contact_number = get_field('contact_number');
$linkedin_profile = get_field('linkedin_url_profile');
$location = get_field('location_select');
$industry = get_field('industry_select');
?>

<section class="container no-pad-gutters expert-profile">
    <div class="back mb-4 mb-md-5">
        <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a
            href="/" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
    </div>
    <div class="row">
        <div class="col-12 col-md-4">
            <!--May implement the expert's profile here -->
            <div class="expert-profile-image">
            <?php 
            if ($profile_image) : ?>
                <img class="w-100" src="<?php echo esc_url($profile_image['url']); ?>" alt="<?php echo esc_attr($profile_image['alt']); ?>">
            <?php endif; ?>
        </div>
        </div>

        <!--May implement the expert's industry expertise here -->
        <div class="col-12 col-md-8">
            <h1 class="text-uppercase mb-3"><?php echo esc_html(get_the_title()) ?></h1>
            <h3 class="expert-position text-uppercase"><?php echo esc_html($position_title); ?></h3>
            <div class="expert-location"><i class="fa fa-lg fa-map-marker mr-2 my-4"></i><?php echo esc_html($location['label']); ?></div>
            <div class="social-icons mb-4">
                <ul class="experts-socials p-0">
                    <li><a href="mailto:<?php echo esc_attr($expert_email); ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                    <li><a href="tel:<?php echo esc_attr($contact_number); ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo esc_attr($linkedin_profile); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="expert-profile-content">
                <?php echo $expert->post_content ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();