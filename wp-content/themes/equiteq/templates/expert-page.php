<?php
/* Template Name: Expert Page */
get_header();
$id = get_the_ID();
$page = get_post($id);
?>

<?php

/**Hero */
hm_get_template_part('template-parts/hero', ['page' => $page]);

?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4"><?php echo esc_html(get_the_title()) ?></h3>
        <div class="row">
            <div class="col-md-8 mb-4">
                <?php echo $page->post_content ?>
            </div>
        </div>
        <!--May implement the search and filter here-->
        <div class="row">
            <div class="col-md-8">
                <div class="filter-title">
                    <h5>
                        FILTERS
                    </h5>
                </div>
                <div class="row">
                    <!-- Filter for industries -->
                    <div class="industry-filter d-inline-block">
                        <div class="dropdown">
                            <button class="industry-filter-toggle dropdown-toggle" type="button" id="industryDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                All Sectors
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="industryDropdown">
                                <?php
                                $industries = get_industries();
                                if (!empty($industries)) :
                                    foreach ($industries as $industry) :
                                        $permalink = get_permalink($industry);
                                        $slug = basename($permalink);
                                        $industry_icon = get_field('industry_icon', $industry);
                                        echo '<li>';
                                        echo '<a class="dropdown-item" href="#" data-industry="' . esc_attr($slug) . '">';
                                        if ($industry_icon) {
                                            echo '<img class="mr-4" src="' . esc_url($industry_icon['url']) . '" alt="' . esc_attr($industry_icon['alt']) . '" width="20px">';
                                        }
                                        echo esc_html(get_the_title($industry));
                                        echo '</a></li>';
                                    endforeach;
                                endif;                                                              
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="location-filter d-inline-block">
                        <!-- Filter for locations -->
                        <div class="dropdown">
                            <button class="location-filter-toggle dropdown-toggle" type="button" id="locationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                All Locations
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="locationDropdown">
                                <?php
                                $locations = get_locations();
                                if (!empty($locations)) :
                                    foreach ($locations as $location) :
                                        $permalink = get_permalink($location);
                                        $slug = basename($permalink);
                                        echo '<li><a class="dropdown-item" href="#" data-location="' . esc_attr($slug) . '">' . esc_html(get_the_title($location)) . '</a></li>';
                                    endforeach;
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <!-- Search box -->
                <div class="team-search">
                    <label for="search" class="searchtext">Search</label>
                    <div class="input-group alt">
                    <input type="text" class="" id="quicksearch">
                    <span class="input-group-append">
                        <div class=""><i class="fa fa-search"></i></div>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--May implement the experts profile list here-->
<section class="team-experts container">
    <div class="row">
        <?php
        $experts = get_experts();

        if (!empty($experts)) :
            foreach ($experts as $expert) :
                // Store permalink in a variable before ACF fields retrieval
                $expert_permalink = get_permalink($expert->ID);
                $expert_title = get_the_title($expert->ID);

                $profile_image = get_field('profile_image', $expert->ID);
                $position_title = get_field('position_title', $expert->ID);
                $expert_email = get_field('expert_email', $expert->ID);
                $contact_number = get_field('contact_number', $expert->ID);
                $linkedin_profile = get_field('linkedin_url_profile', $expert->ID);
                $location = get_field('location_select', $expert->ID);
                
                // Get the location permalink and extract the slug
                $location_permalink = get_permalink($location['value']);
                $location_slug = basename($location_permalink);
                
                $industries = get_field('industry_select', $expert->ID);

                $industry_slugs = array();
                foreach ($industries as $industry) {
                    $industry_post = get_post($industry['value']);
                    $industry_slug = $industry_post->post_name;
                    array_push($industry_slugs, $industry_slug);
                }
                $industry_str = implode(",", $industry_slugs);

                ?>
               <div class="team-box col-12 col-md-3" data-industry="<?php echo esc_attr($industry_str); ?>" data-location="<?php echo esc_attr($location_slug); ?>">
                    <div class="team-box-inner">
                    <div class="team-img">
                        <a href="<?php echo esc_url($expert_permalink); ?>">
                        <img src="<?php echo esc_url($profile_image['url']); ?>" alt="<?php echo esc_attr($profile_image['alt']); ?>">              
                        </a>
                    </div>
                    <div class="member-name">
                        <a href="<?php echo esc_url($expert_permalink); ?>"><?php echo esc_html($expert_title); ?></a>
                    </div>
                    <div class="member-designation"><?php echo esc_html($position_title); ?></div>
                    <div class="member-address"><?php echo esc_html($location['label']); ?></div>
                    <div class="social-icons">
                        <ul class="experts-socials p-0 align-items-center">
                        <li><a href="mailto:<?php echo esc_attr($expert_email); ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="tel:<?php echo esc_attr($contact_number); ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo esc_attr($linkedin_profile); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    </div>
                </div>
                <?php
            endforeach;
            wp_reset_postdata();
        else :
            ?>
            <p>No experts found.</p>
            <?php
        endif;
        ?>
    </div>
</section>

<?php
get_footer();
?>
